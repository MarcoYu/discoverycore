package com.estacionar.finder;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashSet;
import java.util.Set;

import com.estacionar.interfaz.Notificador;

public class FinderNotificadorImplementation {
	
	public FinderNotificadorImplementation() {
		
	}
	
	@SuppressWarnings({ "all" })
	public	Set<Object> findClasses(String path, String paquete) {
		
		String ruta = generarRuta(path, paquete);
		Set<Object> result = new HashSet<Object>();
		
		for (File f : new File(ruta).listFiles()) {
			String nombre = f.getName();
			
			if (!nombre.endsWith(".class")) 
				continue;
			
			Class clazz = null;
			
			try {
				String[] nombreClase = nombre.split("\\.");
//				c = Class.forName(nombreClase[0]);
//				System.out.println(paquete+"NotificadorMail");
//				c = Class.forName(paquete+"NotificadorMails");
//				c = Class.forName(nombre);
				File file = new File(path);
				URL[] cp = {file.toURI().toURL()};
				URLClassLoader urlcl = new URLClassLoader(cp);
				clazz = urlcl.loadClass(paquete+nombreClase[0]);
			} catch (ClassNotFoundException | MalformedURLException e) {
				e.printStackTrace();
			}
			
			if (!Notificador.class.isAssignableFrom(clazz))
				throw new RuntimeException();
			try {
				result.add( clazz.getDeclaredConstructor().newInstance() );
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}

	private String generarRuta(String path, String paquete) {
		StringBuilder ret = new StringBuilder();
		ret.append(path);
		
		if(paquete.isEmpty())
			return ret.toString();
		
		String[] result = paquete.split("\\.");
		
		for (int i=0; i<result.length;i++) {
			ret.append("\\"+result[i]);
		}
		
		return ret.toString();
	}

	
}
