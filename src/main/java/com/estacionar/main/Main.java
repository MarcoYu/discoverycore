package com.estacionar.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.estacionar.finder.FinderNotificadorImplementation;
import com.estacionar.interfaz.Notificador;

public class Main {

	public static void main(String[] args) {
		String path = "C:\\Users\\Marco\\Desktop\\workspace-discovery\\ProyectoExtension\\bin";
		String packagePath = "com.estacionar.impl.";
		
		FinderNotificadorImplementation discovery = new FinderNotificadorImplementation();
		Set<Object> result = discovery.findClasses(path, packagePath);
		
//		List<Object> listadoDeImplementaciones = new ArrayList<Object>(result);
//		Notificador notificador = (Notificador) listadoDeImplementaciones.get(0);
//		notificador.notificar();
		
		List<Object> listadoDeImplementaciones = new ArrayList<Object>(result);
		Notificador notificador = (Notificador) listadoDeImplementaciones.get(0);
		notificador.notificar();
	}

}
